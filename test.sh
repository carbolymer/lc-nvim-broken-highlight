#!/bin/sh
# if you don't have podman installed, you can replace podman with docker commands below
podman build . -t lc-neovim && \
  podman run --rm -it \
  --user 1000 \
  lc-neovim \
  vim -u vimrc test.py
