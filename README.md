# Reproduction steps

Prerequisite: podman installed

1. Run `./test.sh` which will build the container image and start virtual environment
1. Wait for language server to start and observe the following effect:

![](./invalid_syntax.png)

