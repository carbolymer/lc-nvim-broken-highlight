FROM docker.io/archlinux:base-20210131.0.14634
RUN pacman -Syu --noconfirm && \
    pacman -Sy git rsync sudo gvim python python-pip python-language-server yapf python-rope python-pyflakes  python-pycodestyle python-pylint --noconfirm
RUN groupadd -g 1000 work && useradd -m -u 1000 -g work  work && \
    usermod -aG wheel work
RUN echo '%wheel ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
RUN mkdir -p /home/work/.vim/colors
WORKDIR /home/work/
ADD plug.vim .
ADD adventurous.vim .vim/colors/
ADD vimrc .
ADD requirements.txt .
ADD test.py .
RUN chown -R work:work /home/work
USER work
ENV TERM xterm-256color
RUN python -m venv venv && source venv/bin/activate && pip install -r requirements.txt
RUN /usr/bin/vim -u vimrc +PlugInstall +qa
CMD /usr/bin/bash
