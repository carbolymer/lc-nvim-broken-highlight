source /usr/share/vim/vim82/defaults.vim
scriptencoding utf-8
set encoding=utf-8
syntax on
set expandtab
set number
set wildmenu
set wildmode=full
set hlsearch
set number relativenumber
colorscheme adventurous
set signcolumn=yes

source ./plug.vim
call plug#begin('plugged')

Plug 'autozimu/LanguageClient-neovim', { 'branch': 'next', 'do': 'bash install.sh' }

call plug#end()

let g:LanguageClient_serverCommands = {'python': ['pyls'] }

function LC_maps()
  let g:LanguageClient_documentHighlightDisplay = {}
  let g:LanguageClient_loggingFile = '/tmp/LanguageClient.log'
  let g:LanguageClient_serverStderr = '/tmp/LanguageClient_stderr.log'
endfunction

autocmd FileType * call LC_maps()
